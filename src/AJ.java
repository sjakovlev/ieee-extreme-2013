import java.io.PrintStream;
import java.util.LinkedList;
import java.util.Scanner;


public class AJ {

	private final static Scanner IN = new Scanner(System.in);
	private final static PrintStream OUT = System.out;
	
	public static void main(String[] args) {
		LinkedList<Integer> speeds = new LinkedList<Integer>();
		int n = IN.nextInt();
		int speed;
		for (int i = 0; i < n; i++) {
			speed = IN.nextInt();
			if (!speeds.isEmpty()) {
				speeds.add(Math.max(0, speed + speeds.peekLast()));
			} else {
				speeds.add(Math.max(0, speed));
			}
			
		}
		
//		int position = -1;
//		int lastSpeed;
//		for (int i = 0; i < n; i++) {
//			lastSpeed = speeds.pop();
//			if (lastSpeed == 0) {
//				position = -1;
//			} else if (position == -1) {
//				position = i;
//			}
//		}
		OUT.println(speeds.peekLast());
		
	}

}
