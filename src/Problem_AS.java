import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;


public class Problem_AS {
	private final static Scanner IN = new Scanner(System.in);
	private final static PrintStream OUT = System.out;
	private final static int start = 'F' - 65;
	private static int paths = 0;
	private static int shortestSize = Integer.MAX_VALUE;
	private static ArrayList<Integer> shortestPath;
	
	private static void DFS(int[][] graph, int start, int target, ArrayList<Integer> path) {
		int currentVertex = path.get(path.size() - 1);
		
		for (int i = 0; i < graph[currentVertex].length; i++) {
			if (graph[currentVertex][i] != -1 && !path.contains(Integer.valueOf(i))) {
				ArrayList<Integer> nl = (ArrayList<Integer>)path.clone();
				nl.add(i);
				if (i == target) {
					paths += 1;
					if (nl.size() < shortestSize) {
						shortestSize = nl.size();
						shortestPath = nl;
					}
				} else {
					DFS(graph, start, target, nl);
				}
			}
		}
		
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int nearestVendor = IN.nextLine().charAt(0) - 65;
		String nextLine;
		int[][] graph = new int[26][26];
		
		for (int i = 0; i < graph.length; i++) {
			for (int j = 0; j < graph[0].length; j++) {
				graph[i][j] = -1;
			}
		}
		
		int v1;
		int v2;
		String[] tmp;
		while (IN.hasNext()) {
			nextLine = IN.nextLine().trim();
			if (nextLine.length() == 0) break;
			
			tmp = nextLine.split(" ");
			v1 = tmp[0].charAt(0) - 65;
			v2 = tmp[1].charAt(0) - 65;
			graph[v1][v2] = 1;
			graph[v2][v1] = 1;
		}
		ArrayList<Integer> p = new ArrayList<Integer>();
		p.add(start);
		DFS(graph, start, nearestVendor, p);
		if (paths == 0) {
			OUT.printf("No Route Available from %c to %c", start + 65, nearestVendor + 65);
		} else {
			OUT.println("Total Routes: " + paths);
			OUT.println("Shortest Route Length: " + shortestSize);
			OUT.print("Shortest Route after Sorting of Routes of length " + shortestSize + ":");
			Iterator<Integer> it = shortestPath.iterator();
			while (it.hasNext()) {
				OUT.printf(" %c", it.next() + 65);
			}
		}
		IN.close();
		OUT.close();
		
	}

}
