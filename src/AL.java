import java.io.PrintStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Scanner;


public class AL {

	private final static Scanner IN = new Scanner(System.in);
	private final static PrintStream OUT = System.out;
	public static void main(String[] args) {
		int n = Integer.valueOf(IN.nextLine());
		String collection = IN.nextLine();
		int permutations = n;
		Map<Character, LinkedList<Integer>> positions = new HashMap<Character, LinkedList<Integer>>();
		
		Character ch;
		LinkedList<Integer> chPos;
		Iterator<Integer> posIt;
		int distance;
		
		for (int i = 0; i < n; i++) {
			ch = collection.charAt(i);
			if (positions.containsKey(ch)) {
				chPos = positions.get(ch);
				posIt = chPos.iterator();
				while (posIt.hasNext()) {
					distance = i - posIt.next();
					permutations = (permutations + distance) % 12345678;
				}
				chPos.add(i);
			} else {
				chPos = new LinkedList<Integer>();
				chPos.add(i);
				positions.put(ch, chPos);
			}
		}
		OUT.println(permutations);
		IN.close();
		OUT.close();

	}

}
