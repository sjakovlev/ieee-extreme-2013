import java.io.PrintStream;
import java.lang.Character.Subset;
import java.util.HashSet;
import java.util.Scanner;

import javax.security.auth.Subject;


public class AN {

	private final static Scanner IN = new Scanner(System.in);
	private final static PrintStream OUT = System.out;
	
	private static String cryptotext;
	private static HashSet<String> dictionary;
	private static  int maxWordLen;
	
	private static void decode(String in, StringBuilder out, int key) {
		int dec;
		for (int i = 0; i < in.length(); i++) {
			dec = in.charAt(i) - 65 - key;
			while (dec < 0) {
				dec += 26;
			}
			while (dec >= 26) {
				dec -= 26;
			}
			out.append((char)(dec + 65));
		}
	}
	
	private static String decode(int blockSize, int k1, int k2, int k3) {
		int len = cryptotext.length();
		int i = 0;
		StringBuilder out = new StringBuilder();
		int[] k = new int[] {k1, k2, k3};
		int kn = 0;
		int sbstrTo = 0;
		while (i < len) {
			try {
				sbstrTo = Math.min(i + blockSize, cryptotext.length());
				decode(cryptotext.substring(i, sbstrTo), out, k[kn]);
			} catch (Exception e) {
				e.printStackTrace();
				OUT.print(i + "" + blockSize + " " + sbstrTo);
			}
			i += blockSize;
			kn += 1;
			kn %= 3;
		}
		return out.toString();
	}
	
	private static int[] decode() {
		for (int i = 1; i < 11; i++) {
			for (int j = 0; j < 26; j++) {
				for (int j2 = 0; j2 < 26; j2++) {
					for (int k = 0; k < 26; k++) {
						if (check(decode(i, j, j2, k), 0)) {
							return new int[] {i, j, j2, k};
						}
					}
				}
			}
		}
		return null;
	}
	
	private static boolean check(String message, int curPos){
		int maxLen = Math.min(maxWordLen, message.length() - curPos);
		if (maxLen == 0) return true;
		for (int i = 1; i <= maxLen; i++) {
			if (dictionary.contains(message.substring(curPos, curPos + i))) {
				if (check(message, curPos + i)) return true;
			}
		}
		return false;
		
	}
	
	public static void main(String[] args) {
		
		cryptotext = IN.nextLine();
		IN.nextLine();
		
		int dictionaryLen = Integer.valueOf(IN.nextLine());
		maxWordLen = 0;
		
		String[] dict = IN.nextLine().split(" ");
		
		dictionary = new HashSet<>();
		for (int i = 0; i < dict.length; i++) {
			if (dict[i].length() > maxWordLen) maxWordLen = dict[i].length();
			dictionary.add(dict[i]);
		}
		int[] res = decode();
		OUT.println(res[0]);
		OUT.println(res[1]);
		OUT.println(res[2]);
		OUT.println(res[3]);
		OUT.println(decode(res[0], res[1], res[2], res[3]));
		IN.close();
		OUT.close();

	}
}
