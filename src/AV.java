import java.io.PrintStream;
import java.util.Deque;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Scanner;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.xml.stream.events.StartDocument;


public class AV {

	private final static class Coordinates implements Comparable<Coordinates> {
		private int row;
		private int col;
		private int type;
		private int distance;
		private int distanceFromOasis;
		
		public Coordinates(int row, int col, int type, int distance) {
			super();
			this.row = row;
			this.col = col;
			this.type = type;
			this.distance = distance;
		}
		
		public int getRow() {
			return row;
		}

		public int getCol() {
			return col;
		}

		public int getType() {
			return type;
		}

		public int getDistance() {
			return distance;
		}
		
		public void setDistance(int distance) {
			this.distance = distance;
		}

		public int costFunction() {
			int dx = end.getCol() - getCol();
			int dy = end.getRow() - getRow();
			int cost = (dx + dy * timeStr - Math.min(dx, dy) * (timeStr * 2 - timeDiag));
			return getDistance() + cost;
		}

		@Override
		public int compareTo(Coordinates o) {
			if (costFunction() != o.costFunction()) {
				return costFunction() - o.costFunction();
			} else if (getRow() != o.getRow()) {
				return getRow() - o.getRow();
			} else {
				return getCol() - o.getCol();
			}
		}
		
		@Override
		public int hashCode() {
			// TODO Auto-generated method stub
			return getRow() + getCol() * 10240;
		}
		
		public Set<Coordinates> getNeighbours() {
			Set<Coordinates> res = new HashSet<>();
			if (getRow() != 0 && getCol() != 0) {
				res.add(new Coordinates(getRow() - 1, getCol() - 1, map[getRow()-1][getCol()-1], -1));
			}
			if (getRow() != 0) {
				res.add(new Coordinates(getRow() - 1, getCol(), map[getRow()-1][getCol()], -1));
			}
			if (getRow() != 0 && getCol() != map[0].length - 1) {
				res.add(new Coordinates(getRow() - 1, getCol() + 1, map[getRow()-1][getCol()+1], -1));
			}
			if (getCol() != 0) {
				res.add(new Coordinates(getRow(), getCol() - 1, map[getRow()][getCol()-1], -1));
			}
			if (getCol() != map[0].length - 1) {
				res.add(new Coordinates(getRow(), getCol() + 1, map[getRow()][getCol()+1], -1));
			}
			if (getRow() != map.length - 1 && getCol() != 0) {
				res.add(new Coordinates(getRow() + 1, getCol() - 1, map[getRow()+1][getCol()-1], -1));
			}
			if (getRow() != map.length - 1) {
				res.add(new Coordinates(getRow() + 1, getCol(), map[getRow()+1][getCol()], -1));
			}
			if (getRow() != map.length - 1 && getCol() != map[0].length - 1) {
				res.add(new Coordinates(getRow() + 1, getCol() + 1, map[getRow()+1][getCol()+1], -1));
			}
			
			return res;
		}
		
	}
	
	private final static Scanner IN = new Scanner(System.in);
	private final static PrintStream OUT = System.out;
	
	private final static int D = 0;
	private final static int S = 1;
	private final static int E = 2;
	private final static int O = 3;
	
	private final static int timeDiag = 3;
	private final static int timeStr = 2;
	private final static int timeOasis = 2;
	private final static int koeff = 2;
	
	private static Coordinates start;
	private static Coordinates end;
	
	private static int[][] map;
	
	private static int dist(Coordinates a, Coordinates b) {
		if (a.getCol() != b.getCol() && a.getRow() != b.getRow()) return timeDiag;
		else return timeStr;
	}
	
	private static void genPath() {
		int rows = map.length;
		int cols = map[0].length;
		
		Coordinates currentPosition;
		Coordinates tmpPosition;
		Set<Coordinates> visited = new HashSet<Coordinates>();
		SortedSet<Coordinates> queue = new TreeSet<Coordinates>();
		queue.add(start);
		visited.add(start);
		
		Set<Coordinates> neigh;
		Iterator<Coordinates> it;
		int row, col, dist;
 		while (!queue.isEmpty()) {
 			currentPosition = queue.first();
 			queue.remove(currentPosition);
 			row = currentPosition.getRow();
 			col = currentPosition.getCol();
 			dist = currentPosition.getDistance();
 			neigh = currentPosition.getNeighbours();
 			it = neigh.iterator();
 			while (it.hasNext()) {
 				tmpPosition = it.next();
 				if (!visited.contains(tmpPosition)) {
 					tmpPosition.setDistance(currentPosition.getDistance() + dist(currentPosition, tmpPosition));
 					queue.add(tmpPosition);
 					visited.add(tmpPosition);
 				}
 			}
 		}
		
	}
	
	public static void main(String[] args) {
		String buf = IN.nextLine();
		String[] bufArray = buf.split(" ");
		int rows = Integer.valueOf(bufArray[0]);
		int cols = Integer.valueOf(bufArray[1]);
		
		map = new int[rows][cols];
		
		for (int i = 0; i < rows; i++) {
			buf = IN.nextLine();
			for (int j = 0; j < buf.length(); j++) {
				switch (buf.charAt(j)) {
				case 'D':
					map[i][j] = D;
					break;
				case 'S':
					map[i][j] = S;
					start = new Coordinates(i, j, S, 0);
					break;
				case 'E':
					map[i][j] = E;
					end = new Coordinates(i, j, E, -1);
					break;
				case '+':
					map[i][j] = O;
					break;
				default:
					break;
				}
			}
		}
		IN.close();
		
		OUT.close();

	}

}
