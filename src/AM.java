import java.io.PrintStream;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.SortedSet;
import java.util.TreeSet;


public class AM {
	
	private static class Coordinates implements Comparable<Coordinates> {

		private int row;
		private int col;
		private int price;
		
		
		public Coordinates(int row, int col, int price) {
			super();
			this.row = row;
			this.col = col;
			this.price = price;
		}

		public int getRow() {
			return row;
		}

		public int getCol() {
			return col;
		}

		public int getPrice() {
			return price;
		}

		@Override
		public int compareTo(Coordinates o) {
			if (getPrice() != o.getPrice()) {
				return getPrice() - o.getPrice();
			} else if (getPrice() < o.getPrice()) {
				return -1;
			} else if (getRow() != o.getRow()) {
				return getRow() - o.getRow();
			} else {
				return getCol() - o.getCol();
			}
		}
		
		@Override
		public String toString() {
			return "[" + getRow() + "," + getCol() + "]";
			
		}
		
	}
	
	
	
	private final static Scanner IN = new Scanner(System.in);
	private final static PrintStream OUT = System.out;
	
	private static int outerPrice;
	
	private static void printField(int[][] field) {
		for (int i = 0; i < field.length; i++) {
			for (int j = 0; j < field[0].length; j++) {
				OUT.printf("%2d", field[i][j]);
			}
			OUT.println();
		}
	}
	
	private static List<Coordinates> find(int[][] field) {
		
		int rows = field.length;
		int cols = field[0].length;
		
		Coordinates[][] parents = new Coordinates[rows][cols];
		boolean[][] visited = new boolean[rows][cols];
		SortedSet<Coordinates> queue = new TreeSet<Coordinates>();
		
		Coordinates tmpCoord;
		
		for (int i = 0; i < field[rows - 1].length; i++) {
			tmpCoord = new Coordinates(field.length-1, i, field[rows-1][i]);
			queue.add(tmpCoord);
		}
		
		int row, col, price;
		int newRow, newCol, newPrice;
		
		while (!queue.isEmpty()) {
			tmpCoord = queue.first();
			queue.remove(tmpCoord);
			row = tmpCoord.getRow();
			col = tmpCoord.getCol();
			price = tmpCoord.getPrice();
			visited[row][col] = true;
			//OUT.println(row + " " + col + " " + price);
			if (row == 0) {
				// Done. Traceback.
				
				List<Coordinates> result = new LinkedList<Coordinates>();
				Coordinates next = tmpCoord;
				result.add(next);
				while (next.getRow() != rows - 1) {
					next = parents[next.getRow()][next.getCol()];
					result.add(next);
				}
				//OUT.println(price);
				outerPrice = price;
				return result;
				
			} else {
				newRow = row - 1;
				
				// Left
				if (col != 0) {
					newCol = col - 1;
					if (!visited[newRow][newCol]) {
						newPrice = price + field[newRow][newCol];
						visited[newRow][newCol] = true;
						queue.add(new Coordinates(newRow, newCol, newPrice));
						parents[newRow][newCol] = tmpCoord;
					}
				}
				
				// Middle
				newCol = col;
				if (!visited[newRow][newCol]) {
					newPrice = price + field[newRow][newCol];
					visited[newRow][newCol] = true;
					queue.add(new Coordinates(newRow, newCol, newPrice));
					parents[newRow][newCol] = tmpCoord;
				}
				
				// Right
				if (col != cols - 1) {
					newCol = col + 1;
					if (!visited[newRow][newCol]) {
						newPrice = price + field[newRow][newCol];
						visited[newRow][newCol] = true;
						queue.add(new Coordinates(newRow, newCol, newPrice));
						parents[newRow][newCol] = tmpCoord;
					}
				}
			}
		}
		return null;
	}
	
	public static void main(String[] args) {
		
		String input;
		String[] inputParsed;
		
		inputParsed = IN.nextLine().split(" ");
		int rows = Integer.valueOf(inputParsed[0]);
		int cols = Integer.valueOf(inputParsed[1]);
		
		int[][] field = new int[rows][cols];
		
		for (int i = 0; i < rows; i++) {
			input = IN.nextLine();
			inputParsed = input.split(" ");
			for (int j = 0; j < cols; j++) {
				field[i][j] = Integer.valueOf(inputParsed[j]);
			}
		}
		IN.close();
		
		List<Coordinates> result = find(field);
		
		OUT.print("Minimum risk path = ");
		Iterator<Coordinates> it = result.iterator();
		while (it.hasNext()) {
			OUT.print(it.next());
		}
		OUT.println();
		OUT.print("Risks along the path = " + outerPrice);
		OUT.close();
	}

}
