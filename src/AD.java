import java.io.PrintStream;
import java.util.Collections;
import java.util.Scanner;


public class AD {

	private final static Scanner IN = new Scanner(System.in);
	private final static PrintStream OUT = System.out;
	
	private static int c2i(char c) {
		if (48 <= c && c <= 58) return c - 48;
		if (64 <= c && c <= 90) return c - 64;
		if (97 <= c && c <= 123) return c - 96;
		return -1;
	}
	
	private static char i2c(int c) {
		return (char) (c + 64);
	}
	
	private static String encryptWord(String key1, String num1, String key2, String num2, String word) {
		char[] ark1 = key1.toCharArray();
		char[] ark2 = key2.toCharArray();
		char[] an1 = num1.toCharArray();
		char[] an2 = num2.toCharArray();
		char[] amsg = word.toCharArray();
		char[] res = new char[amsg.length];
		for (int i = 0; i < amsg.length; i++) {
			res[i] = i2c((c2i(ark1[i]) * c2i(an1[i]) + c2i(ark2[i]) * c2i(an2[i]) + c2i(amsg[i]))%26 +1); 
		}
		return String.valueOf(res);
		
	}
	public static void main(String[] args) {
		
		//OUT.println(encryptWord("GOODMORNINGEVERYBODY", "23419584736458392039", "GOODEVENINGTOYOUTOOO", "95837264758253647583", "are"));
		String buf;
		String[] bufAr;
		char[] word;
		char[] encodedWord;
		int maxLen = 0;
		int key[] = new int[31];
		int k;
		
		while (IN.hasNext()) {
			buf = IN.nextLine();
			if (buf.equals(".")) break;
			bufAr = buf.split(" ");
			word = bufAr[0].toCharArray();
			encodedWord = bufAr[1].toCharArray();
			
			if (word.length > maxLen) maxLen = word.length;
			
			for (int i = 0; i < word.length; i++) {
				if (encodedWord[i] > word[i]) k = c2i(encodedWord[i]) - c2i(word[i]) - 1;
				else k = c2i(encodedWord[i]) - c2i(word[i]) + 25;
				
				if (key[i] != 0 && key[i] != k) {
					key[i] = -1;
				} else if (key[i] != -1) {
					key[i] = k;
				}
			}
		}
		IN.close();
		
		int[] k2 = new int[maxLen];
		int[] n2 = new int[maxLen];
		int rem;
		
		for (int i = 0; i < maxLen; i++) {
			if (key[i] == -1) {
				k2[i] = -1;
				n2[i] = -1;
				continue;
			}
			rem = key[i] - 1;
			if (rem < 1) rem += 26;
			for (int j = 1; j < 26; j++) {
				if (rem % j == 0 && rem / j < 10) {
					n2[i] = rem / j;
					k2[i] = j;
					break;
				}
				if ((rem + 26) % j == 0 && (rem + 26) / j < 10) {
					n2[i] = (rem + 26) / j;
					k2[i] = j;
					break;
				}
				if ((rem + 52) % j == 0 && (rem + 52) / j < 10) {
					n2[i] = (rem + 52) / j;
					k2[i] = j;
					break;
				}
			}
		}
		
		for (int i = 0; i < maxLen; i++) {
			if (k2[i] != -1) OUT.print("A");
			else OUT.print("?");
		}
		OUT.println();
		for (int i = 0; i < maxLen; i++) {
			if (k2[i] != -1) OUT.print("1");
			else OUT.print("?");
		}
		OUT.println();
		for (int i = 0; i < maxLen; i++) {
			if (k2[i] != -1) OUT.print(i2c(k2[i]));
			else OUT.print("?");
		}
		OUT.println();
		for (int i = 0; i < maxLen; i++) {
			if (n2[i] != -1) OUT.print(n2[i]);
			else OUT.print("?");
		}
		OUT.close();

	}

}
