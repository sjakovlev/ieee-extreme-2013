import java.io.*;
import java.util.*;

public class Solution {
	private final static Scanner IN = new Scanner(System.in);
	private final static PrintStream OUT = System.out;
	
    public static void main(String[] args) {
        
    	String[] input = IN.nextLine().split(" ");
    	IN.close();
    	if (input.length > 20) {
    		OUT.println("ERROR");
			return;
    	}
    	Deque<Integer> stack = new LinkedList<Integer>();
    	
    	for (int i = 0; i < input.length; i++) {
    		if (input[i].equals("+")) {
    			try{
    				stack.push(Math.min(stack.pop() + stack.pop(), 0xFFFF));
    			} catch (NoSuchElementException e) {
    				OUT.println("ERROR");
    				return;
    			}
    		} else if (input[i].equals("-")) {
    			try{
    				int arg1 = stack.pop();
    				stack.push(Math.max(stack.pop() - arg1, 0));
    			} catch (NoSuchElementException e) {
    				OUT.println("ERROR");
    				return;
    			}
    		} else if (input[i].equals("&")) {
    			try{
    				stack.push(stack.pop() & stack.pop());
    			} catch (NoSuchElementException e) {
    				OUT.println("ERROR");
    				return;
    			}
    		} else if (input[i].equals("|")) {
    			try{
    				stack.push(stack.pop() | stack.pop());
    			} catch (NoSuchElementException e) {
    				OUT.println("ERROR");
    				return;
    			}
    		} else if (input[i].equals("~")) {
    			try{
    				stack.push((~stack.pop()) & 0x0000FFFF);
    			} catch (NoSuchElementException e) {
    				OUT.println("ERROR");
    				return;
    			}
    		
    		} else if (input[i].equals("X")) {
    			try{
    				stack.push(stack.pop() ^ stack.pop());
    			} catch (NoSuchElementException e) {
    				OUT.println("ERROR");
    				return;
    			}
    		} else {
    			try{
    				if (input[i].charAt(0) == '-') {
    					OUT.println("ERROR");
        				return;
    				}
    				if (Integer.valueOf(input[i], 16) < 0 || Integer.valueOf(input[i], 16) > 0xFFFF) {
    					OUT.println("ERROR");
        				return;
    				}
    				stack.push(Math.min(Math.max(Integer.valueOf(input[i], 16), 0), 0xFFFF));
    			} catch (NumberFormatException e) {
    				OUT.println("ERROR");
    				return;
    			}
    		}
    	}
    	if (stack.size() == 1) {
    		OUT.println(String.format("%4s", Integer.toHexString(stack.pop()).toUpperCase()).replace(" ", "0"));
    	} else {
    		OUT.println("ERROR");
    	}
    }
}