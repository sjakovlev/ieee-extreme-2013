import java.io.PrintStream;
import java.math.BigInteger;
import java.util.Scanner;


public class SolutionRobot {
	private final static Scanner IN = new Scanner(System.in);
	private final static PrintStream OUT = System.out;
	
	private final static BigInteger M = new BigInteger("4294967296");
	private final static BigInteger A = BigInteger.valueOf(1664525);
	private final static BigInteger C = BigInteger.valueOf(1013904223);
	private final static int wallN = 0b0001;
	private final static int wallS = 0b0010;
	private final static int wallE = 0b0100;
	private final static int wallW = 0b1000;
	private final static int N = wallS|wallE|wallW;
	private final static int S = wallN|wallE|wallW;
	private final static int E = wallN|wallS|wallW;
	private final static int W = wallN|wallS|wallE;
	
	
	private static BigInteger random_x;
	
	private static void initRandom(BigInteger seed) {
		random_x = seed;
	}
	
	private static int nextRandom() {
		random_x = A.multiply(random_x).add(C).remainder(M);
		OUT.println(random_x.intValue());
		return random_x.intValue();
	}
	
	private static int nextRandom(int a, int b) {
		return Math.abs(a + (nextRandom() % (b - a + 1)));
	}
	
	private static int[] selectBorderCell(int rows, int cols, boolean excludeCell, int[] excludedCell) {
		int numOfBorderCells = 2 * cols + 2 * rows - 8;
		int selectedCell = nextRandom(1, numOfBorderCells - (excludeCell ? 1 : 0)); //Considering TL corner and excluded cell
		
		if (excludeCell && selectedCell >= (excludedCell[0] * cols + excludedCell[1])) selectedCell += 1;
		
		if (selectedCell < cols - 1){
			// In first row
			//return selectedCell;
			return new int[] {0, selectedCell};
		} else {
			selectedCell -= (cols - 1);
			int div = selectedCell / 2;
			int rem = selectedCell % 2;
			if (div < rows - 2) {
				// In the middle
				//return cols * div + (rem == 0 ? 0 : (cols - 1));
				return new int[] {div + 1, rem == 0 ? 0 : (cols - 1)};
			} else {
				// Last row
				selectedCell -= (rows - 2) * 2;
				//return cols * (rows - 1) + selectedCell + 1; 
				return new int[] {rows - 1, selectedCell};
			}
		}
	}
	
	private static int[][] generateField(int rows, int cols) {
		int[][] field = new int[rows][cols];
		for (int i = 0; i < field.length; i++) {
			for (int j = 0; j < field[i].length; j++) {
				field[i][j] = 0b1111;
			}
		}
		return field;
	}
	
	private static void setCellWalls(int[][] field, int row, int col, int newWall) {
		field[row][col] = field[row][col] & newWall;
		switch (newWall) {
		case N:
			if (row > 0) field[row-1][col] 	= field[row-1][col] & S;
			break;
		case S:
			if (row < field.length - 1) field[row+1][col] = field[row+1][col] & N;
			break;
		case W:
			if (col > 0) field[row][col-1] = field[row][col-1] & E;
			break;
		case E:
			if (col < field[0].length - 1) field[row][col+1] = field[row][col+1] & W;
			break;

		default:
			break;
		}
//		if (row > 0) 					field[row-1][col] 	= field[row-1][col] & S | ((newWall & wallN) != 0 ? wallS : 0);
//		if (row < field.length - 1) 	field[row+1][col] 	= field[row+1][col] & N | ((newWall & wallS) != 0 ? wallN : 0);
//		if (col > 0)					field[row][col-1] = field[row][col-1]	& E | ((newWall & wallW) != 0 ? wallE : 0);
//		if (col < field[0].length - 1)	field[row][col+1] = field[row][col+1] 	& W | ((newWall & wallE) != 0 ? wallW : 0);
	}
	
	private static void generateInnerCells(int[][] field) {
		for (int i = 1; i < field.length - 1; i++) {
			for (int j = 1; j < field[0].length - 1; j++) {
				int rand = nextRandom(1, 4);
				switch (rand) {
				case 1:
					setCellWalls(field, i, j, N);
					break;
				case 2:
					setCellWalls(field, i, j, S);
					break;
				case 3:
					setCellWalls(field, i, j, W);
					break;
				case 4:
					setCellWalls(field, i, j, E);
					break;
				default:
					break;
				}
			}
		}
	}
	
	private static void printField(int[][] field) {
		String[] w = {};
		for (int i = 0; i < field.length; i++) {
			for (int j = 0; j < field[0].length; j++) {
				OUT.printf("%5s", Integer.toBinaryString(field[i][j]));
			}
			OUT.println();
		}
	}
	
	public static void main(String[] args) {
		int seed = IN.nextInt();
		int rows = IN.nextInt();
		int cols = IN.nextInt();
		int pwall = IN.nextInt();
		int moves = IN.nextInt();
		
		initRandom(BigInteger.valueOf(seed));
		
		for (int i = 0; i < 20; i++) {
			//OUT.println(nextRandom(1,  4));
		}
		int[] startPosition = selectBorderCell(rows, cols, false, null);
		int[] exitPosition = selectBorderCell(rows, cols, true, startPosition);
		int[][] field = generateField(rows, cols);
		OUT.println(startPosition[0] + " " + startPosition[1]);
		OUT.println(exitPosition[0] + " " + exitPosition[1]);
		if (exitPosition[0] == 0) setCellWalls(field, exitPosition[0], exitPosition[1], N);
		else if (exitPosition[0] == rows - 1) setCellWalls(field, exitPosition[0], exitPosition[1], S);
		else if (exitPosition[1] == 0) setCellWalls(field, exitPosition[0], exitPosition[1], W);
		else if (exitPosition[1] == cols - 1) setCellWalls(field, exitPosition[0], exitPosition[1], E);
		else OUT.print("NOOOOO");
		
		generateInnerCells(field);
		printField(field);
		IN.close();
	}

}
