import java.io.PrintStream;
import java.util.Scanner;


public class HKN {
	private final static Scanner IN = new Scanner(System.in);
	private final static PrintStream OUT = System.out;
	
	private static int palin_simple(int binaryDigits) {
		if (binaryDigits == 1) return 2;
		int freedom = (int) (Math.ceil(binaryDigits/2.0) - 1);
		return (int)Math.pow(2, freedom) + palin_simple(binaryDigits - 1);
	}
	private static boolean is_palin (String a, int digits) {
		for (int i = 0; i < digits/2; i++) {
			if(a.charAt(i) != a.charAt(digits - 1 - i)) {
				return false;
			}
		}
		return true;
	}
	
	private static int palin(int n) {
		int binaryDigits = (int) (Math.floor(Math.log(n) / Math.log(2)) + 1);
		int res = palin_simple(binaryDigits - 1);
		
		int mi = (int) Math.pow(2, binaryDigits - 1);
		for (int i = mi; i <= n; i++) {
			if (is_palin(Integer.toBinaryString(i), binaryDigits)) {
				res += 1;
			}
		}
		//OUT.println(freedom);
		//res += (int)Math.pow(2, freedom);
		return res;
	}
	
	public static void main(String[] args) {
		String[] input = IN.nextLine().split(",");
		int a = Integer.valueOf(input[0]);
		int b = Integer.valueOf(input[1]);
		
		OUT.println(palin(b) - palin(a-1));
	}
}
