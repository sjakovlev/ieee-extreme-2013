import java.io.PrintStream;
import java.util.Scanner;


public class AK {

	private final static Scanner IN = new Scanner(System.in);
	private final static PrintStream OUT = System.out;
	
	private static void printGraph(boolean[][] graph) {
		for (int i = 0; i < graph.length; i++) {
			for (int j = 0; j < graph.length; j++) {
				OUT.print(graph[i][j] ? 1 : 0);
			}
			OUT.println();
		}
	}
	public static void main(String[] args) {
		
		int n = IN.nextInt();
		
		boolean[][] graph = new boolean[n][n];
		
		boolean[] outflow = new boolean[n];
		boolean[] inflow = new boolean[n];
		
		for (int i = 0; i < inflow.length; i++) {
			inflow[i] = true;
		}
		
		for (int i = 0; i < outflow.length; i++) {
			outflow[i] = true;
		}
		
		int a, b;
		for (int i = 0; i < n - 1; i++) {
			a = IN.nextInt() - 1;
			b = IN.nextInt() - 1;
			graph[a][b] = true;
			inflow[b] = false;
			outflow[a] = false;
		}
		
		int[] load = new int[n];
		
		int next;
		for (int i = 0; i < inflow.length; i++) {
			if (inflow[i]){
				next = i; 
				while (true) {
					load[next]++;
					//OUT.print(">" + next);
					if (outflow[next]) break;
					for (int j = 0; j < graph[next].length; j++) {
						if (graph[next][j]) {
							next = j;
							break;
						}
					}
				}
			}
		}
		
		for (int i = 0; i < outflow.length; i++) {
			if (outflow[i]){
				next = i; 
				while (true) {
					load[next]++;
					//OUT.print(">" + next);
					if (inflow[next]) break;
					for (int j = 0; j < graph[next].length; j++) {
						if (graph[j][next]) {
							next = j;
							break;
						}
					}
				}
			}
		}


		IN.close();
		
		//printGraph(graph);
		
		int maxLoad = 0;
		for (int i = 0; i < load.length; i++) {
			if (load[i] > maxLoad) maxLoad = load[i];
		}
		
		for (int i = 0; i < load.length; i++) {
			if (load[i] == maxLoad) OUT.println(i + 1);
		}
		OUT.close();

	}
}
