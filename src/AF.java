import java.io.BufferedOutputStream;
import java.io.PrintStream;
import java.util.Scanner;


public class AF {

	private final static Scanner IN = new Scanner(System.in);
	private final static PrintStream OUT = new PrintStream(new BufferedOutputStream(System.out));
	
	private static int gen(int num) {
		int pos = 1;
		int step = 1;
		for (int i = 1; i < num; i++) {
			if (i % 7 == 0 || String.valueOf(i).contains("7")){
				//OUT.println("Change dir. " + i);
				step *= -1;
			}
			pos += step;
			if (pos == 0) pos = 1337;
			if (pos == 1338) pos = 1;
		}
		return pos;
	}
	
	public static void main(String[] args) {
		

		int testNum = IN.nextInt();
		int favNum;
		
		for (int i = 0; i < testNum; i++) {
			favNum = IN.nextInt();
			OUT.println(gen(favNum));
		}
		IN.close();
		OUT.close();

	}

}
