import java.io.PrintStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Scanner;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;


public class IEEETV {
	private static class Tile implements Comparable<Tile> {

		private int id;
		private int width;
		private int height;
		
		public Tile(int id, int width, int height) {
			super();
			this.id = id;
			this.width = width;
			this.height = height;
		}

		public int getId() {
			return id;
		}

		public int getWidth() {
			return width;
		}
		public int getHeight() {
			return height;
		}
		
		@Override
		public int compareTo(Tile o) {
			if (getWidth() != o.getWidth()) {
				return getWidth() - o.getWidth();
			} else if (getHeight() != o.getHeight()) {
				return getHeight() - o.getHeight();
			} else {
				return getId() - o.getId();
			}
		}
		
		private Tile findClosest(SortedSet<Tile> tiles, int width) {
			Iterator<Tile> it = tiles.iterator();
			Tile curTile;
			while(it.hasNext()) {
				curTile = it.next();
				if (curTile.width >= width) {
					tiles.remove(curTile);
					return curTile;
				}
			}
			return null;
		}
		
		public void coverWithTiles(Tile tileType) {
			char filler = 'X';
			int curWidth = 0;
			int curHeight = 0;
			String[] structure = new String[getHeight()];
			SortedSet<Tile> tiles = new TreeSet<Tile>();
			
			while (curWidth != getWidth() || curHeight != getHeight()) {
				int needToCover = getWidth() - curHeight;
				if (needToCover >= tileType.getWidth()) {
					for (int i = curHeight; i < curHeight + tileType.getHeight(); i++) {
						structure[i] += String.format("%"+needToCover+"s", filler).replace(' ', filler);
					}
				} else {
					Tile usable = findClosest(tiles, curWidth);
					for (int i = curHeight; i < curHeight + tileType.getHeight(); i++) {
						structure[i] += String.format("%"+needToCover+"s", filler).replace(' ', filler);
					}
				}
			}
			
			
		}
		
	}
	
	private static class Room {
		private char id;
		private int tileId;
		private int width;
		private int height;
		public Room(char id, int tileId, int width, int height) {
			super();
			this.id = id;
			this.tileId = tileId;
			this.width = width;
			this.height = height;
		}
		public char getId() {
			return id;
		}
		public int getTileId() {
			return tileId;
		}
		public int getWidth() {
			return width;
		}
		public int getHeight() {
			return height;
		}
	}

	private final static Scanner IN = new Scanner(System.in);
	private final static PrintStream OUT = System.out;
	public static void main(String[] args) {
		int numOfTiles = IN.nextInt();
		int numOfRooms = IN.nextInt();
		
		Map<Integer, Tile> tileTypes = new HashMap<>();
		
		LinkedList<Room> rooms = new LinkedList<Room>();
		
		int id;
		for (int i = 0; i < numOfTiles; i++) {
			id = IN.nextInt();
			tileTypes.put(id, new Tile(id, IN.nextInt(), IN.nextInt()));
		}
		
		for (int i = 0; i < numOfRooms; i++) {
			rooms.add(new Room(IN.next().charAt(0), IN.nextInt(), IN.nextInt(), IN.nextInt()));
		}
		
		

		IN.close();
		OUT.close();

	}

}
